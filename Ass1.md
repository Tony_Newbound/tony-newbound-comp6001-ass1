#1. Getting Sick Isn’t Fun. Go Figure!

After a bit of experimentation, we came to the conclusion that people won’t enjoy VR experiences that make them feel sick. Makes sense! We found that we could eliminate motion sickness almost entirely with a high 
enough frame-rate and by limiting motion. We found a few tricks for moving the player around that reduced sickness. For instance, linear motion makes people much less sick than acceleration does. Rotating the horizon
makes almost everyone sick, so don't do that! But nothing works quite as well as simply letting the player stay still. There are different types of movement in the game wether is teleportaion to a selected area, teleportaion
to a designated area or there is free (which creates motionsickness)

[Source](https://madewith.unity.com/stories/secrets-of-a-super-spy-vr-game).



#2. User Interface Design

 
Oculus suggests that user interfaces in VR should fit inside the middle 1/3rd of the user’s viewing area. Remember players won’t move their head  they should be able to examine it with head movements.

The last thing you want them to be doing is straining there eyes. Depending on the platform your aiming for, making a simple 'Safe Area' template, like this may help in design your UI elements.


When your in VR it can feel so real that you can get lost in your own mind, while this is a good thisng it can also be a bad thing, after a few hours of constant VR gaming you feel exausted not only physically but mentally

so avoiding putting anymore strain on your eyes and brain will make peoples experience alot better.

[Source](https://madewith.unity.com/stories/development-update-7-exploring-the-virtual).


#3. Controlling the Player


As players are wearing HMD's (Head Mounted Display), they will generally have to rely on tactile feedback (or trying keys) to find controls. So if you’re going with a keyboard for input, try thinking about familiar keys or easy to find keys.

Some developers have noted that a joy-pad with analogue sticks works the best.

Aim for realistic movements rather than an exaggerated control setup.

Look-to-select. Use head movement itself as a direct control over world interactions. This isn’t always a possibility though and again it depends on the experience your creating.

Depending on your game and the experience your aiming for, tracked controllers such as the Oculus touch may work for manipulating objects in VR.

[Source](https://madewith.unity.com/stories/development-update-7-exploring-the-virtual).


There are multiple ways to controler the player with with teleportaion or sliding along the ground or being able to spawn at pre set target. All 3 ways bring new dynamics to VR as it can become quite overbearing with the 

wrong types of controls form a certain game.


#4. Research a lot

In the beginning of the project, we dedicated some time for research. We searched for everything we could find about VR. From John Carmack’s talks to some VR gameplays, we tried to absorb everything out there to create the 

best experience we could. For everyone that asks me which are the best talks to watch about designing for VR, I always recommend this two: VR Design: Transitioning from a 2D to 3D Design Paradigm, by Alex Chu, VR Interface Design 

Pre-Visualisation Methods, by Mike Alger. These two talks (among others) helped us a lot to understand more about ergonomics and field range. Although there is no manual of good practices, we can learn a lot with the sharing researches 

out there.

[Source](https://madewith.unity.com/stories/the-experience-of-finding-monsters-adventure-for-gear-vr).



#5. Legibility according to distance

It’s well known that in most of the cases, the information should always be legible. In our case, there were some texts, like tutorial, that players should be able to see well. In VR, text distance may vary according to the size of your environment and assets. 

If you are in a small room, your assets should be at least as far as the wall of the room. 

[Source](https://madewith.unity.com/stories/the-experience-of-finding-monsters-adventure-for-gear-vr).

If the assets are much further than the wall this may happen: This goes without saying usin Vr is already straining on the brain and if the player has to struggle to also 

see the intructions or read simple text then the overall game experience witll be poor.


#6. Information in the front

In our tests, having too much information attached to the player’s HUD was uncomfortable and unnecessary. It didn’t feel natural. The players enter a whole new different world in VR, they want to be able to look around and see

the details. So, as information were way more comfortable in world camera, we placed most of our assets right in front of the player.

[Source](https://madewith.unity.com/stories/the-experience-of-finding-monsters-adventure-for-gear-vr).

This helps to keep the emmersion alive if there are boxes filled with text on the screen it can feel 

very enclosed and over whelming but having the info infront of the player and able to acces it when they require make for a more enjoyable and stramline gameplay.

#7. Better have an excess of feedbacks than few

People barely know VR yet. So, to avoid people getting lost with it, we’ve created a lot of feedbacks for each player action.

Crosshair : Our crosshair has 2 states: the idle and the Clickable UI. The first one is a little dot to give the player notion where is the center, so they can drag it over the clickable elements. And the second one, the “Clickable UI”, appears 

every time the player has it’s aim over a button. 

[Source](https://madewith.unity.com/stories/the-experience-of-finding-monsters-adventure-for-gear-vr).

This will help people playing the game know when they can or have interacted with an object this makes it alot easier to do at speed when there are enemys rushing towards you (say in a tower defense game)


#8. Don’t suppose anything without proving it right  

In the beginning of the project, we had a screen that should give the players the information needed to play a level. As we were placing it in front of the camera, we were testing it in Unity itself and it seemed good and legible. But, in the minute we made a build 

and tested it, the position was all wrong. Texts were too bright, the information was closer than expected and as it was so close, we even felt a little claustrophobic. 

[Source](https://madewith.unity.com/stories/the-experience-of-finding-monsters-adventure-for-gear-vr).

This is a really important principle as this needs to be done after every stage in the creation 

process, it could be as simple as adding a monster to fight but without going into VR and testing the mster there is no way to tell if the scale or animationsof the said asset will be correct.


## Peer Review:
I enjoyed using Tony’s application. The VR experience is new to me, yet I was able to easily and quickly workout how to use the app.

The instructions were clear and easy to read, the controls were simple and easy to grasp.

The environment itself was beautiful, I especially like being in the universe, it was not only aesthetically pleasing but also simple and comfortable to navigate. I like the hidden Easter egg that activated once all sliders were maxed. A very pleasant experience overall, Thank Tony.

Peer Review:
Johnny Kingi / 10005351





